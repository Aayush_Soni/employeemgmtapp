package com.EmployeeMgmt.app.Controller;


import com.EmployeeMgmt.app.Service.EmployeeService;
import com.EmployeeMgmt.app.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public List<Employee> getAllEmployee(){
        return employeeService.getAllEmployee();
    }

    @PostMapping
    public String addEmployee(@RequestBody Employee employee){
           return employeeService.addEmployee(employee);
    }

    @GetMapping("{id}")
    public Employee getEmployee(@PathVariable int id){
        return employeeService.getEmpById(id);
    }
















}
