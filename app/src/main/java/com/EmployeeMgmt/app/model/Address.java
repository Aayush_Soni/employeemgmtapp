package com.EmployeeMgmt.app.model;

import javax.persistence.*;

@Entity
@Table(name = "ADDRESS")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int addressId;
    @Column
    private String addr;
    @Column
    private String city;
    @Column
    private String country;
//
//    public Address() {
//    }
//
//    public Address(String address, String city, String country) {
//
//        this.addr = address;
//        this.city = city;
//        this.country = country;
//    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

//    @Override
//    public String toString() {
//        return "Address{" +
//                "addressId=" + addressId +
//                ", address='" + addr + '\'' +
//                ", city='" + city + '\'' +
//                ", country='" + country + '\'' +
//                '}';
//    }
}
