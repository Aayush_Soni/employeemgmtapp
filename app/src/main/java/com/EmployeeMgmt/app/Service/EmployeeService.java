package com.EmployeeMgmt.app.Service;

import com.EmployeeMgmt.app.model.Employee;

import java.util.List;

public interface EmployeeService{

    public List<Employee> getAllEmployee();
    public Employee getEmpById(int id);
    public String addEmployee(Employee employee);
}
