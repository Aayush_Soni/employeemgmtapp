package com.EmployeeMgmt.app.Service;

import com.EmployeeMgmt.app.Repository.EmployeeRepository;
import com.EmployeeMgmt.app.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmployee() {
        List<Employee> emps = employeeRepository.findAll();

        return emps;
    }

    @Override
    public Employee getEmpById(int id) {
       return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public String addEmployee(Employee employee) {
        Employee emp = employeeRepository.save(employee);
        return "Employee saved successfully with id:"+emp.getId();
    }
}
