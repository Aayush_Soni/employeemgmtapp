package com.EmployeeMgmt.app;

import com.EmployeeMgmt.app.Repository.EmployeeRepository;
import com.EmployeeMgmt.app.model.Address;
import com.EmployeeMgmt.app.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppApplication  {

	public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
	}

//	@Autowired
//	private EmployeeRepository employeeRepo;
//	@Override
//	public void run(String... args) throws Exception {
//		Address a1 = new Address("a-colony","hyderabad","India");
//		Address a2 = new Address("b-colony","bangalore","India");
//
//		Employee e1 = new Employee();
//		e1.setName("Pawan");
//		e1.setDesignation("Software Engineer");
//		e1.getAddresses().add(a1);
//		e1.getAddresses().add(a2);
//		employeeRepo.save(e1);
//	}
}
